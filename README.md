# SecureChatServer
Python Flask Server with encrypted messaging

## Usage
For usage on your local machine / network

- pip install virtualenv

### Install
- Run: `make install`

### Run
- Issue: `make run`

### Test
- Issue: `make test`

### Clean (prior to checkin)
- Issue: `make clean`

### Virtual Env Manually
- From within SecureChatServer directory
- (optional for python venv) python3 -m venv venv
- (optional for python venv) Linux: source venv/bin/activate
- (optional for python venv) Windows: venv\Scripts\activate
- (optional for python venv) After finished usage, deactivate venv by issuing: `deactivate` from within SecureChatServer directory

#### To create dependencies.txt
- dependencies.txt created using: `pip freeze > dependencies.txt` within python virtual environment.

## Interface
### Local 
- From webbrowser, navigate to url: localhost:5000 (or whatever IP and port you ran the server with)
  - NOTE: If you use incognito mode for browser, you can run multiple sessions (browser
    won't share data amongst windows); therefore, you can login with multiple users

### Common
- Select 'User Actions / Login' button
- Add a username
- Enter the password: `enter1the2chat3room4`
  - All users have the same password
- Select "Add User" button
- Again enter the login credentials and select "Login"
- From user page, enable or disable features and navigate to chat page if permitted

## Notes
- Encryption / Decryption / All User Login Password: enter1the2chat3room4

## Structure
.<br>
├── dependencies.txt<br>
├── LICENSE<br>
├── makefile<br>
├── README.md<br>
├── run.py<br>
├── src<br>
│   ├── config<br>
│   │   ├── config.json<br>
│   │   ├── server_config.py<br>
│   │   └── version.txt<br>
│   ├── database<br>
│   │   └── models.py<br>
│   ├── __init__.py<br>
│   ├── static<br>
│   │   ├── chat.css<br>
│   │   ├── home.css<br>
│   │   └── js<br>
│   │       ├── chat.js<br>
│   │       └── home.js<br>
│   ├── templates<br>
│   │   ├── chat.html<br>
│   │   ├── home.html<br>
│   │   ├── ssh_key_loader.html<br>
│   │   └── user_action.html<br>
│   ├── utils<br>
│   │   ├── encryption_tools.py<br>
│   └── views.py<br>
└── tests<br>

## TODO
- SSH authentication page
- Chat page
  - Clean up the datatype conversions on booleans between javascript and python
  - Adjust the messageContainer to not be cut off 
  - Increase error handling in javascript for the chat page
  - Error checking if not able to access crypto-js library

## TESTS
- TBD


