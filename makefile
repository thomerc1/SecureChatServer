SRC_DIR := src

# default target to run (if no parameters passed to make cmd)
install: venv
	pip install -Ur dependencies.txt

test: venv
	pytest -v -s

run: venv
	python3 run.py	

venv: 
	python3 -m venv venv
	. venv/bin/activate

clean: ## Clean up
	-rm -rf venv
	-rm -rf .pytest_cache
	-rm -rf __pycache__
	-rm -rf $(SRC_DIR)/__pycache__
	-rm -rf $(SRC_DIR)/config/__pycache__
	-rm -rf $(SRC_DIR)/database/__pycache__
	-rm -rf $(SRC_DIR)/utils/__pycache__
	-rm -rf tests/__pycache__
	-rm $(SRC_DIR)/database/server.db
