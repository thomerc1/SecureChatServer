# __init__.py
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from .database.models import db
import os

# Create flask app
app = Flask(__name__)

# Set a secret key for your application
app.secret_key = os.urandom(24)

# Configure the default database URI
cwd = os.path.abspath(os.path.dirname(__file__))
database_uri = 'sqlite:///' + os.path.join(cwd, 'database', 'server.db')
app.config['SQLALCHEMY_DATABASE_URI'] = database_uri

# Initialize SQLAlchemy instance
db.init_app(app)

with app.app_context():
    db.create_all()

# autopep8: off
from . import views
# autopep8: on