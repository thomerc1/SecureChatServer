# test_encryption_tools.py

import pytest
import os
import sys
from utils.encryption_tools import encrypt_data_with_password, decrypt_data_with_password, get_password_hash


@pytest.fixture
def password():
    return "test_password"


@pytest.fixture
def data():
    return b"Hello, World!"


@pytest.fixture
def hashed_password(password):
    return get_password_hash(password)


def test_get_password_hash(password, hashed_password):
    # Verify that the hashed password matches the expected format and length
    assert isinstance(hashed_password, str)
    assert len(hashed_password) == 64  # Length of a SHA-256 hex digest


def test_encrypt_data_with_password(data, password):
    # Verify that data encryption returns bytes and that encrypted data is not the same as the original data
    encrypted_data = encrypt_data_with_password(data, password)
    assert isinstance(encrypted_data, bytes)
    assert encrypted_data != data


def test_decrypt_data_with_password(data, password):
    # Verify that encrypted data can be decrypted back to the original data
    encrypted_data = encrypt_data_with_password(data, password)
    decrypted_data = decrypt_data_with_password(encrypted_data, password)
    assert decrypted_data == data


def test_encryption_decryption_integration(data, password):
    # Integration test to verify that encryption and decryption work together as expected
    encrypted_data = encrypt_data_with_password(data, password)
    decrypted_data = decrypt_data_with_password(encrypted_data, password)
    print(f"{decrypted_data}, {data}")
    assert decrypted_data == data


def test_hash_validation(password, hashed_password):
    # Verify that the password hash matches the expected hash for the given password
    assert get_password_hash(password) == hashed_password
