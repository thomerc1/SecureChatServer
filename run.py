from src import app
from src.database.models import UsersModel, ChatModel
import argparse
from socket import inet_aton

if __name__ == '__main__':
    # Log all users out on startup
    UsersModel.set_all_users_logged_out(app)

    # Setup argument parser
    parser = argparse.ArgumentParser(description="Run the web application.")
    parser.add_argument('--ip', type=str, help='The IP address to bind to.', default='127.0.0.1')
    parser.add_argument('--port', type=int, help='The port to listen on.', default=5000)
    args = parser.parse_args()

    # Validate IP address
    try:
        inet_aton(args.ip)
    except OSError:
        print("Error: Invalid IP address format.")
        sys.exit(1)

    # Validate port number
    if not (0 <= args.port <= 65535):
        print("Error: Port number must be between 0 and 65535.")
        sys.exit(1)

    # Run the application
    app.run(host=args.ip, port=args.port, debug=True)
